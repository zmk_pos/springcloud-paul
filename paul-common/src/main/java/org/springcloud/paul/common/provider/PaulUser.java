package org.springcloud.paul.common.provider;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 17:09
 * @description
 */
@Data
public class PaulUser {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(
            hidden = true
    )
    private String clientId;
    @ApiModelProperty(
            hidden = true
    )
    private Long userId;
    @ApiModelProperty(
            hidden = true
    )
    private String tenantId;
    @ApiModelProperty(
            hidden = true
    )
    private String userName;
    @ApiModelProperty(
            hidden = true
    )
    private String account;
    @ApiModelProperty(
            hidden = true
    )
    private String roleId;
    @ApiModelProperty(
            hidden = true
    )
    private String roleName;
}
