package org.springcloud.paul.common.consts;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:04
 * @description
 */
public interface AppConstant {
    String APPLICATION_VERSION = "1.0.0";
    String BASE_PACKAGES = "org.springcloud.paul";
    String APPLICATION_NAME_PREFIX = "paul-";
    String APPLICATION_GATEWAY_NAME = "paul-gateway";
    String APPLICATION_CUSTOMER_NAME = "customer-service";
    String APPLICATION_AUTH_NAME = "auth-service";
    String APPLICATION_ORDER_NAME = "order-service";
    String APPLICATION_PRODUCT_NAME = "product-service";
    String DEV_CODE = "dev";
    String PROD_CODE = "prod";
    String TEST_CODE = "test";
    String OS_NAME_LINUX = "LINUX";
}
