package org.springcloud.paul.common.bean;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/31 15:57
 * @description
 */
public class PageResult<T> {
    public PageResult(Long total, List<T> items) {
        this.total = total;
        this.items = items;
    }

    private Long total;
    private List<T> items;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}