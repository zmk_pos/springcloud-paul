package org.springcloud.paul.common.enums;

import lombok.Getter;

@Getter
public enum AuthEnum {
    BEARER("Bearer", 6, 7),
    TOKEN("Token", 5, 6);

    String key;
    int val;
    int yval;

    AuthEnum(String key, int val, int yval) {
        this.key = key;
        this.val = val;
        this.yval = yval;
    }

}
