package org.springcloud.paul.common.consts;

public class SystemConst {
    public static final String DEFAULTPWD = "1234";
    public static final String PAGENUM = "1";  //默认页码
    public static final String PAGESIZE = "15"; //每页大小
    public static final String ADMINNAME = "超级管理员";
    public static final String ADMIN="admin";
    public static final String USERLOGIN="用户登录";
}
