package org.springcloud.paul.common.provider;

import lombok.Data;

/**
 * @author zmk
 * @version 1.0
 * @date 2020/10/21 17:26
 * @description
 */
@Data
public class TokenInfo {

    /**
     * 令牌值
     */
    private String token;

    /**
     * 过期秒数
     */
    private Long expire;
}