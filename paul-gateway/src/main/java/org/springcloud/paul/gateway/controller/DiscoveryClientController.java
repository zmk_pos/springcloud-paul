package org.springcloud.paul.gateway.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 16:51
 * @description
 */
@Api(description = "在线服务接口")
@RestController
@RequestMapping("/discovery")
public class DiscoveryClientController {

    private DiscoveryClient discoveryClient;

    /**
     * 获取服务实例
     */
    @ApiOperation(value = "获取服务实例列表", notes = "获取服务实例列表", httpMethod = "GET")
    @GetMapping("/instances")
    public Map<String, List<ServiceInstance>> instances() {
        Map<String, List<ServiceInstance>> instances = new HashMap<>(16);
        List<String> services = discoveryClient.getServices();
        services.forEach(s -> {
            List<ServiceInstance> list = discoveryClient.getInstances(s);
            instances.put(s, list);
        });
        return instances;
    }

}