package org.springcloud.paul.gateway.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.ArrayList;
import java.util.List;

/**
 * 路由配置类
 *
 * Created by zmk523@163.com on 2020/4/18 10:25
 */
@Data
@RefreshScope
@ConfigurationProperties("paul.document")
public class RouteProperties {

	private final List<RouteResource> resources = new ArrayList<>();

}
