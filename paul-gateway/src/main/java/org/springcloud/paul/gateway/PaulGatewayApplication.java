package org.springcloud.paul.gateway;

import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableCircuitBreaker
@SpringCloudApplication
@EnableDiscoveryClient
public class PaulGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaulGatewayApplication.class, args);
    }

    @Bean
    @LoadBalanced
    @SentinelRestTemplate
    /**
     * 负载均衡
     */
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
