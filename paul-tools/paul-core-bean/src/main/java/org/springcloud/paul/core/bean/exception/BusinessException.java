package org.springcloud.paul.core.bean.exception;

public class BusinessException extends RuntimeException {
    private String msg;

    public String getMsg() {
        return msg;
    }

    private Integer code;

    public Integer getCode() {
        return code;
    }

    private Object data;

    public Object getData() {
        return data;
    }

    public BusinessException(ExceptionCode exceptionCode) {
        this.code = exceptionCode.value();
        this.msg = exceptionCode.getDescribe();
        this.data = null;
    }

    public BusinessException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public BusinessException(String msg) {
        this.code = ExceptionCode.PARAMETER_ERROR.value();
        this.msg = msg;
        this.data = null;
    }
}

