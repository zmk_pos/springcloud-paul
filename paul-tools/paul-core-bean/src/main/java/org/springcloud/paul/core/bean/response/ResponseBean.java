package org.springcloud.paul.core.bean.response;

import org.springcloud.paul.core.bean.exception.ExceptionCode;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/6 14:52
 * @description
 */
public class ResponseBean<T> {
    private int code;

    private String msg;

    private T data;

    public ResponseBean() {

    }

    public ResponseBean(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    /**
     * @param <T> T 泛型标记
     * @return R
     */
    public static <T> ResponseBean<T> success(T data) {
        return success("", data);
    }

    /**
     * @param msg 消息
     * @param <T> T 泛型标记
     * @return R
     */
    public static <T> ResponseBean<T> success(String msg, T data) {
        return new ResponseBean<>(ExceptionCode.NORMAL.value(), msg, data);
    }

    /**
     * @param msg 消息
     * @param <T> T 泛型标记
     * @return R
     */
    public static <T> ResponseBean<T> fail(String msg) {
        return fail(ExceptionCode.REQUEST_EXCEPTION.value(), msg);
    }

    /**
     * @param code 状态码
     * @param msg  消息
     * @param <T>  T 泛型标记
     * @return R
     */
    public static <T> ResponseBean<T> fail(ExceptionCode code, String msg) {
        return fail(code.value(), msg);
    }

    /**
     * @param code 状态码
     * @param msg  消息
     * @param <T>  T 泛型标记
     * @return R
     */
    public static <T> ResponseBean<T> fail(int code, String msg) {
        return new ResponseBean<>(code, msg, null);
    }
}
