package org.springcloud.paul.core.bean.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 打印 Feign 日志与配置参数结合
 * logging:level:
 * # feign日志以什么级别监控哪个接口
 * org.springcloud.paul.order.api.feign.IOrderClient: DEBUG
 *
 * @author zmk
 * @version 1.0
 * @date 2021/4/6 10:01
 * @description
 */
@Configuration
public class FeignConfig {
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
