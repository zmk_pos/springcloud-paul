package org.springcloud.paul.sharding.standard;

import lombok.SneakyThrows;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.springcloud.paul.sharding.utils.DateUtil;

import java.text.ParseException;
import java.util.Collection;

/**
 * 日期精确分片
 *
 * @author zmk
 * @version 1.0
 * @date 2021/4/28 15:11
 * @description
 */
public class DayShardingAlgorithm implements PreciseShardingAlgorithm<String> {

    /**
     * 设置分片
     *
     * @param tableNames    数据表
     * @param shardingValue 分片列信息
     * @return
     */
    @SneakyThrows
    @Override
    public String doSharding(Collection<String> tableNames, PreciseShardingValue<String> shardingValue) {
        String tableName = shardingValue.getLogicTableName();
        String key = DateUtil.getDate(shardingValue.getValue(), 8);
        return tableName.concat("_").concat(key);
    }


}