package org.springcloud.paul.sharding.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/28 15:15
 * @description
 */
public class DateUtil {

    /**
     * 日期转换
     *
     * @param date
     * @return
     */
    public static Date dateFormat(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 得到日期数字
     *
     * @param date 字符串日期
     * @param len  长度
     * @return 202008
     * @throws ParseException
     */
    public static String getDate(String date, int len) throws ParseException {
        String number = date.replaceAll("\\D", "");
        return number.substring(0, len);
    }
}
