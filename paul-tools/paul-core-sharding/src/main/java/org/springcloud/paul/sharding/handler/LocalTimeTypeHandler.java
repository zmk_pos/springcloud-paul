package org.springcloud.paul.sharding.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTime支持
 * 解决 shardingsphere4.1.1，不支持localDate
 *
 * @author zmk
 * @version 1.0
 * @date 2021/4/28 15:06
 * @description
 */
@Component
//定义转换器支持的JAVA类型
@MappedTypes(LocalTime.class)
//定义转换器支持的数据库类型
@MappedJdbcTypes(value = JdbcType.TIMESTAMP, includeNullJdbcType = true)
public class LocalTimeTypeHandler extends BaseTypeHandler<LocalTime> {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, LocalTime localTime, JdbcType jdbcType) throws SQLException {
        if (localTime != null) {
            preparedStatement.setString(i, dateTimeFormatter.format(localTime));
        }
    }

    @Override
    public LocalTime getNullableResult(ResultSet resultSet, String s) throws SQLException {
        String target = resultSet.getString(s);
        if (StringUtils.isEmpty(target)) {
            return null;
        }
        return LocalTime.parse(target, dateTimeFormatter);
    }

    @Override
    public LocalTime getNullableResult(ResultSet resultSet, int i) throws SQLException {
        String target = resultSet.getString(i);
        if (StringUtils.isEmpty(target)) {
            return null;
        }
        return LocalTime.parse(target, dateTimeFormatter);
    }

    @Override
    public LocalTime getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        String target = callableStatement.getString(i);
        if (StringUtils.isEmpty(target)) {
            return null;
        }
        return LocalTime.parse(target, dateTimeFormatter);
    }
}