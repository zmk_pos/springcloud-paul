package org.springcloud.paul.sharding.config;

import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/23 14:14
 * @description
 */
@Configuration
public class KeyIdConfig {

    @Bean("productKeyGenerator")
    public SnowflakeShardingKeyGenerator productKeyGenerator() {
        return new SnowflakeShardingKeyGenerator();
    }

    @Bean("orderKeyGenerator")
    public SnowflakeShardingKeyGenerator orderKeyGenerator() {
        return new SnowflakeShardingKeyGenerator();
    }

    @Bean("customerKeyGenerator")
    public SnowflakeShardingKeyGenerator customerKeyGenerator() {
        return new SnowflakeShardingKeyGenerator();
    }
}
