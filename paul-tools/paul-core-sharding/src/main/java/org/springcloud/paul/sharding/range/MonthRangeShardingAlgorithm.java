package org.springcloud.paul.sharding.range;

import com.google.common.collect.Range;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.springcloud.paul.sharding.utils.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;


/**
 * 月范围分片
 *
 * @author zmk
 * @version 1.0
 * @date 2021/4/28 15:10
 * @description
 */
public class MonthRangeShardingAlgorithm implements RangeShardingAlgorithm<String> {

    /**
     * 设置分片
     *
     * @param collection
     * @param rangeShardingValue
     * @return
     */
    @Override
    public Collection<String> doSharding(Collection<String> collection, RangeShardingValue<String> rangeShardingValue) {
        Collection<String> result = new LinkedHashSet<>();
        DateFormat sdf = new SimpleDateFormat("yyyyMM");
        //日期
        Range<String> ranges = rangeShardingValue.getValueRange();
        Date startTime = DateUtil.dateFormat(ranges.lowerEndpoint());
        Date endTime = DateUtil.dateFormat(ranges.upperEndpoint());

        Calendar cal = Calendar.getInstance();
        while (startTime.getTime() <= endTime.getTime()) {
            String value = sdf.format(startTime);
            for (String each : collection) {
                if (each.endsWith(value)) {
                    result.add(each);
                    break;
                }
            }
            cal.setTime(startTime);
            cal.add(Calendar.MONTH, 1);
            startTime = cal.getTime();
        }
        if (result.size() == 0) {
            result = collection;
        }
        return result;
    }

}