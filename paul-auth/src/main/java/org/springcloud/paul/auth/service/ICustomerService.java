package org.springcloud.paul.auth.service;

import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.customer.api.pojo.Customers;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 17:29
 * @description
 */
public interface ICustomerService {
    ResponseBean<Customers> userInfo(String account, String password);

    ResponseBean<String> degrade() throws Exception;
}
