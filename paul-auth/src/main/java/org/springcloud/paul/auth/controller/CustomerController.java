package org.springcloud.paul.auth.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springcloud.paul.auth.service.ICustomerService;
import org.springcloud.paul.auth.utils.TokenUtil;
import org.springcloud.paul.common.provider.AuthInfo;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 15:56
 * @description
 */
@RestController
@AllArgsConstructor
@Api(value = "用户授权认证", tags = "授权接口")
@RequestMapping("/auth")
public class CustomerController {

    private final ICustomerService customerService;

    @SentinelResource(blockHandler = "SentinelUrlBlockHandler")
    @ApiOperation(value = "客户登录")
    @GetMapping("/login")
    public AuthInfo login(@ApiParam(value = "账号") @RequestParam(required = false) String account
            , @ApiParam(value = "密码") @RequestParam(required = false) String password) {
        ResponseBean<Customers> customersResponseBean = customerService.userInfo(account, password);
        if (null == customersResponseBean.getData() || customersResponseBean.getData().getId() <= 0) {
            throw new BusinessException(customersResponseBean.getCode(), customersResponseBean.getMsg());
        }
        return TokenUtil.createAuthInfo(customersResponseBean.getData());
    }

    @ApiOperation(value = "模拟降级")
    @GetMapping("/degrade")
    public ResponseBean<String> degrade() throws Exception {
        return customerService.degrade();
    }
}
