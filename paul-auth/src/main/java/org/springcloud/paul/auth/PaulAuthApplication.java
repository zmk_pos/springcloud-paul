package org.springcloud.paul.auth;

import org.springcloud.paul.common.consts.AppConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:34
 * @description
 */
@EnableHystrix
@EnableFeignClients(AppConstant.BASE_PACKAGES)
@SpringBootApplication(scanBasePackages = {"org.springcloud.paul.**"})
@EnableDiscoveryClient
public class PaulAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaulAuthApplication.class, args);
    }
}
