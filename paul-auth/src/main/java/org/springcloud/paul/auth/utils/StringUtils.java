package org.springcloud.paul.auth.utils;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 14:34
 * @description
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    /**
     * 强转string,并去掉多余空格
     *
     * @param str 字符串
     * @return String
     */
    public static String toStr(Object str) {
        return toStr(str, "");
    }

    /**
     * 强转string,并去掉多余空格
     *
     * @param str          字符串
     * @param defaultValue 默认值
     * @return String
     */
    public static String toStr(Object str, String defaultValue) {
        if (null == str) {
            return defaultValue;
        }
        return String.valueOf(str);
    }
}
