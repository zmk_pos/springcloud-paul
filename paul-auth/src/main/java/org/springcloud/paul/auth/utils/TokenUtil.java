package org.springcloud.paul.auth.utils;

import org.springcloud.paul.common.consts.TokenConstant;
import org.springcloud.paul.common.provider.AuthInfo;
import org.springcloud.paul.common.provider.TokenInfo;
import org.springcloud.paul.customer.api.pojo.Customers;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 14:08
 * @description
 */
public class TokenUtil {

    /**
     * 创建认证token
     *
     * @param customer 用户信息
     * @return token
     */
    public static AuthInfo createAuthInfo(Customers customer) {
        //设置jwt参数
        Map<String, String> param = new HashMap<>(16);
        param.put(TokenConstant.TOKEN_TYPE, TokenConstant.ACCESS_TOKEN);
        param.put(TokenConstant.TENANT_ID, "0");
        param.put(TokenConstant.OAUTH_ID, "0");
        param.put(TokenConstant.USER_ID, customer.getId() + "");
        param.put(TokenConstant.ROLE_ID, "0");
        param.put(TokenConstant.ACCOUNT, customer.getTelphone());
        param.put(TokenConstant.USER_NAME, customer.getTelphone());
        param.put(TokenConstant.ROLE_NAME, "0");

        TokenInfo accessToken = SecureUtil.createJWT(param, "audience", "issuser", TokenConstant.ACCESS_TOKEN);
        AuthInfo authInfo = new AuthInfo();
        authInfo.setUserId(customer.getId());
        authInfo.setTenantId("0");
        authInfo.setOauthId("0");
        authInfo.setAccount(customer.getTelphone());
        authInfo.setUserName(customer.getNickName());
        authInfo.setAuthority("0");
        authInfo.setAccessToken(accessToken.getToken());
        authInfo.setExpiresIn(accessToken.getExpire());
        authInfo.setRefreshToken(createRefreshToken(customer).getToken());
        authInfo.setTokenType(TokenConstant.BEARER);
        authInfo.setLicense(TokenConstant.LICENSE_NAME);

        return authInfo;
    }

    /**
     * 创建refreshToken
     *
     * @param customer 用户信息
     * @return refreshToken
     */
    private static TokenInfo createRefreshToken(Customers customer) {
        Map<String, String> param = new HashMap<>(16);
        param.put(TokenConstant.TOKEN_TYPE, TokenConstant.REFRESH_TOKEN);
        param.put(TokenConstant.USER_ID, customer.getId() + "");
        return SecureUtil.createJWT(param, "audience", "issuser", TokenConstant.REFRESH_TOKEN);
    }
}
