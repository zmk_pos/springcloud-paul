package org.springcloud.paul.auth.service;

import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.customer.api.feign.ICustomerClient;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 17:29
 * @description
 */
@Service
@Repository
public class ICustomerServiceImpl implements ICustomerService {

    @Autowired
    private ICustomerClient customerClient;

    @Override
    public ResponseBean<Customers> userInfo(String account, String password) {
        return customerClient.userInfo(account, password);
    }

    @Override
    public ResponseBean<String> degrade() throws Exception {
        return customerClient.degrade();
    }


}
