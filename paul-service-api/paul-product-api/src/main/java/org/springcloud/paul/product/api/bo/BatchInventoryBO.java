package org.springcloud.paul.product.api.bo;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/29 14:14
 * @description：
 * @modified By：
 * @version: V-
 */
public class BatchInventoryBO {
    
    private  String tableName;
    private Integer inventory;
    private Long id;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
