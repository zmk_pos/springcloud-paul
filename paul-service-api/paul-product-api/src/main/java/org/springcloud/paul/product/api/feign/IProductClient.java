package org.springcloud.paul.product.api.feign;

import org.springcloud.paul.common.consts.AppConstant;
import org.springcloud.paul.core.bean.handle.FeginInterceptor;
import org.springcloud.paul.product.api.pojo.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:40
 * @description
 */
@FeignClient(
        value = AppConstant.APPLICATION_PRODUCT_NAME,
        path = "/product",
        fallback = IProductClientFallback.class,
        configuration = FeginInterceptor.class
)
public interface IProductClient {

    @PostMapping("/subtractStorage")
    int subtractStorage(@RequestBody List<Product> list);

    @PostMapping("/getProductList")
    List<Product> getProductList(@RequestBody List<String> list);
}
