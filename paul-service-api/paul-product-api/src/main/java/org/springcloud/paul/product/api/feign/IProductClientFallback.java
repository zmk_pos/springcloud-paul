package org.springcloud.paul.product.api.feign;

import org.springcloud.paul.product.api.pojo.Product;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:40
 * @description
 */
@Component
public class IProductClientFallback implements IProductClient {
    @Override
    public int subtractStorage(List<Product> list) {
        return -1;
    }

    @Override
    public List<Product> getProductList(List<String> list) {
        return null;
    }
}
