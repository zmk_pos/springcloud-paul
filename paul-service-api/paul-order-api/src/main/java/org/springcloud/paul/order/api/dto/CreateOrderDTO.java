package org.springcloud.paul.order.api.dto;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:30
 * @description
 */
public class CreateOrderDTO {
    private Long customerId;
    private List<OrderProductsDTO> orderProducts;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<OrderProductsDTO> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<OrderProductsDTO> orderProducts) {
        this.orderProducts = orderProducts;
    }
}
