package org.springcloud.paul.order.api.feign;

import org.springcloud.paul.order.api.dto.CreateOrderDTO;
import org.springframework.stereotype.Component;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/25 11:30
 * @description
 */
@Component
public class IOrderClientFallback implements IOrderClient {
    @Override
    public Long addOrder(CreateOrderDTO orderDTO) {
        return -1L;
    }
}
