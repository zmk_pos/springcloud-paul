package org.springcloud.paul.order.api.feign;

import org.springcloud.paul.common.consts.AppConstant;
import org.springcloud.paul.core.bean.handle.FeginInterceptor;
import org.springcloud.paul.order.api.dto.CreateOrderDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/25 11:30
 * @description
 */
@FeignClient(
        value = AppConstant.APPLICATION_ORDER_NAME,
        path = "/order",
        fallback = IOrderClientFallback.class,
        configuration = FeginInterceptor.class
)
public interface IOrderClient {

    @PostMapping("/createOrder")
    Long addOrder(@RequestBody CreateOrderDTO orderDTO);
}
