package org.springcloud.paul.order.api.dto;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:31
 * @description
 */
public class OrderProductsDTO {
    private String productNo;
    private int quantity;

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
