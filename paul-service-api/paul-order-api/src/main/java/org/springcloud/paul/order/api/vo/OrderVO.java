package org.springcloud.paul.order.api.vo;

import org.springcloud.paul.order.api.pojo.OrderProduct;
import org.springcloud.paul.order.api.pojo.Orders;

import java.io.Serializable;
import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 9:59
 * @description
 */
public class OrderVO extends Orders implements Serializable {
    private List<OrderProduct> orderProducts;

    public List<OrderProduct> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<OrderProduct> orderProducts) {
        this.orderProducts = orderProducts;
    }
}
