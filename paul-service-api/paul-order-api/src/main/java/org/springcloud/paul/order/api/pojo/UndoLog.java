package org.springcloud.paul.order.api.pojo;

import java.io.Serializable;
import java.util.Date;

public class UndoLog implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.id
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.branch_id
     *
     * @mbggenerated
     */
    private Long branchId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.xid
     *
     * @mbggenerated
     */
    private String xid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.context
     *
     * @mbggenerated
     */
    private String context;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.log_status
     *
     * @mbggenerated
     */
    private Integer logStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.log_created
     *
     * @mbggenerated
     */
    private Date logCreated;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.log_modified
     *
     * @mbggenerated
     */
    private Date logModified;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.ext
     *
     * @mbggenerated
     */
    private String ext;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column undo_log.rollback_info
     *
     * @mbggenerated
     */
    private byte[] rollbackInfo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table undo_log
     *
     * @mbggenerated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.id
     *
     * @return the value of undo_log.id
     *
     * @mbggenerated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.id
     *
     * @param id the value for undo_log.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.branch_id
     *
     * @return the value of undo_log.branch_id
     *
     * @mbggenerated
     */
    public Long getBranchId() {
        return branchId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.branch_id
     *
     * @param branchId the value for undo_log.branch_id
     *
     * @mbggenerated
     */
    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.xid
     *
     * @return the value of undo_log.xid
     *
     * @mbggenerated
     */
    public String getXid() {
        return xid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.xid
     *
     * @param xid the value for undo_log.xid
     *
     * @mbggenerated
     */
    public void setXid(String xid) {
        this.xid = xid == null ? null : xid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.context
     *
     * @return the value of undo_log.context
     *
     * @mbggenerated
     */
    public String getContext() {
        return context;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.context
     *
     * @param context the value for undo_log.context
     *
     * @mbggenerated
     */
    public void setContext(String context) {
        this.context = context == null ? null : context.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.log_status
     *
     * @return the value of undo_log.log_status
     *
     * @mbggenerated
     */
    public Integer getLogStatus() {
        return logStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.log_status
     *
     * @param logStatus the value for undo_log.log_status
     *
     * @mbggenerated
     */
    public void setLogStatus(Integer logStatus) {
        this.logStatus = logStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.log_created
     *
     * @return the value of undo_log.log_created
     *
     * @mbggenerated
     */
    public Date getLogCreated() {
        return logCreated;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.log_created
     *
     * @param logCreated the value for undo_log.log_created
     *
     * @mbggenerated
     */
    public void setLogCreated(Date logCreated) {
        this.logCreated = logCreated;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.log_modified
     *
     * @return the value of undo_log.log_modified
     *
     * @mbggenerated
     */
    public Date getLogModified() {
        return logModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.log_modified
     *
     * @param logModified the value for undo_log.log_modified
     *
     * @mbggenerated
     */
    public void setLogModified(Date logModified) {
        this.logModified = logModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.ext
     *
     * @return the value of undo_log.ext
     *
     * @mbggenerated
     */
    public String getExt() {
        return ext;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.ext
     *
     * @param ext the value for undo_log.ext
     *
     * @mbggenerated
     */
    public void setExt(String ext) {
        this.ext = ext == null ? null : ext.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column undo_log.rollback_info
     *
     * @return the value of undo_log.rollback_info
     *
     * @mbggenerated
     */
    public byte[] getRollbackInfo() {
        return rollbackInfo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column undo_log.rollback_info
     *
     * @param rollbackInfo the value for undo_log.rollback_info
     *
     * @mbggenerated
     */
    public void setRollbackInfo(byte[] rollbackInfo) {
        this.rollbackInfo = rollbackInfo;
    }
}