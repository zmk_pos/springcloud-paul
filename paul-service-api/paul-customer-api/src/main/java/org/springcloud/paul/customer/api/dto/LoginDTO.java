package org.springcloud.paul.customer.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:15
 * @description
 */
@Data
@ApiModel("登录")
public class LoginDTO implements Serializable {

    @ApiModelProperty(value = "手机号", required = true)
    private String telPhone;
    @ApiModelProperty(value = "密码", required = true)
    private String password;
}