package org.springcloud.paul.customer.api.feign;

import org.springframework.stereotype.Component;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:38
 * @description
 */
@Component
public class ICustomerAccountClientFallback implements ICustomerAccountClient {
    @Override
    public int subtractAccount(Long customerId, double payable) {
        return -1;
    }
}
