package org.springcloud.paul.customer.api.feign;

import org.springcloud.paul.common.consts.AppConstant;
import org.springcloud.paul.core.bean.handle.FeginInterceptor;
import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:00
 * @description
 */
@FeignClient(
        value = AppConstant.APPLICATION_CUSTOMER_NAME,
        path = "/customer",
        fallback = ICustomerClientFallback.class,
        configuration = FeginInterceptor.class
)
public interface ICustomerClient {

    @GetMapping("/get")
    Customers get(@RequestParam("id") Long id);

    @GetMapping("/login")
    ResponseBean<Customers> userInfo(@RequestParam("account") String account, @RequestParam("password") String password);

    @GetMapping("/degrade")
    ResponseBean<String> degrade();
}
