package org.springcloud.paul.customer.api.feign;

import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.stereotype.Component;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:00
 * @description
 */
@Component
public class ICustomerClientFallback implements ICustomerClient {

    @Override
    public Customers get(Long id) {
        return null;
    }

    @Override
    public ResponseBean<Customers> userInfo(String account, String password) {
        return new ResponseBean<>(200, "服务降级", null);
    }

    @Override
    public ResponseBean<String> degrade() {
        return new ResponseBean<>(200, "降级成功", "降级成功");
    }
}
