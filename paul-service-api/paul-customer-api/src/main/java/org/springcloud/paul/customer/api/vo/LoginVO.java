package org.springcloud.paul.customer.api.vo;

import lombok.Data;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:17
 * @description
 */
@Data
public class LoginVO {
    private String token; // 令牌
    private long expire; // 令牌有效期
    private String key; // 加密密钥
    private String random; // 随机数，服务器生成
    private Long mid;
}
