package org.springcloud.paul.customer.api.feign;

import org.springcloud.paul.common.consts.AppConstant;
import org.springcloud.paul.core.bean.handle.FeginInterceptor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:37
 * @description
 */
@FeignClient(
        value = AppConstant.APPLICATION_CUSTOMER_NAME,
        path = "/customer",
        fallback = ICustomerAccountClientFallback.class,
        configuration = FeginInterceptor.class
)
public interface ICustomerAccountClient {


    @GetMapping("/account/subtractAccount")
    int subtractAccount(@RequestParam("customerId") Long customerId, @RequestParam("payable") double payable);
}
