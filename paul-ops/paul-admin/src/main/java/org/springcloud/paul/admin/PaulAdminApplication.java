package org.springcloud.paul.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:52
 * @description
 */
@EnableAdminServer
@EnableDiscoveryClient
@SpringCloudApplication
public class PaulAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaulAdminApplication.class, args);
    }

}
