package org.springcloud.paul.turbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/23 16:18
 * @description
 */
@EnableHystrix
@SpringBootApplication
@EnableTurbine
@EnableHystrixDashboard
@EnableDiscoveryClient
public class PaulTurbineApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaulTurbineApplication.class, args);
    }
}