package org.springcloud.paul.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:59
 * @description
 */
@EnableEurekaServer
@SpringCloudApplication
public class PaulEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaulEurekaApplication.class, args);
    }

}