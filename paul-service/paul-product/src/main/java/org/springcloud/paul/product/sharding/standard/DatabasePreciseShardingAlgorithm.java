//package org.springcloud.paul.product.sharding.standard;
//
//import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
//import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
//import org.springframework.stereotype.Component;
//
//import java.util.Collection;
//
///**
// * @author zmk
// * @version 1.0
// * @date 2021/4/23 14:41
// * @description
// */
//@Component
//public class DatabasePreciseShardingAlgorithm implements PreciseShardingAlgorithm<Long> {
//    @Override
//    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Long> shardingValue) {
//        System.out.println("------------------select database name");
//        Long curValue = shardingValue.getValue();
//        String curBase = "";
//        if (curValue > 0 && curValue<=200) {
//            curBase = "paul-product-01";
//        } else {
//            curBase = "paul-product-02";
//        }
//        return curBase;
//    }
//}