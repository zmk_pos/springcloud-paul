package org.springcloud.paul.product.service;

import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.product.api.pojo.Product;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:22
 * @description
 */
public interface IProductService {

    int add(Product product);

    int update(Product product);

    Product getById(Long id);

    List<Product> list();

    PageResult<Product> page(Integer pageNum, Integer pageSize);

    int subtractStorage(List<Product> list);

    List<Product> getProductListByCode(List<String> list);
}
