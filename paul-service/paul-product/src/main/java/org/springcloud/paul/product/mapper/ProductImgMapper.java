package org.springcloud.paul.product.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springcloud.paul.product.api.pojo.ProductImg;
import org.springcloud.paul.product.api.pojo.ProductImgExample;

public interface ProductImgMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int countByExample(ProductImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int deleteByExample(ProductImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int insert(ProductImg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int insertSelective(ProductImg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    List<ProductImg> selectByExample(ProductImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    ProductImg selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") ProductImg record, @Param("example") ProductImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") ProductImg record, @Param("example") ProductImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(ProductImg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_img
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(ProductImg record);
}