package org.springcloud.paul.product.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:37
 * @description
 */
@Data
@Component
public class BaseConfig {
    //主键ID生成参数 workerId
    @Value("${snow.flake.worker.id}")
    private Integer snowFlakeWorkerId;

    //主键ID生成参数 dataCenterId
    @Value("${snow.flake.data.center.id}")
    private Integer snowFlakeDataCenterId;
}
