//package org.springcloud.paul.product.config;
//
//import com.alibaba.druid.pool.DruidDataSource;
//import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration;
//import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration;
//import org.apache.shardingsphere.api.config.sharding.strategy.InlineShardingStrategyConfiguration;
//import org.apache.shardingsphere.api.config.sharding.strategy.StandardShardingStrategyConfiguration;
//import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
//import org.springcloud.paul.product.sharding.UserShardingAlgorithm;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.boot.web.servlet.ServletRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Properties;
//
///**
// * @author zmk
// * @version 1.0
// * @date 2021/4/23 11:36
// * @description
// */
//@Configuration
//public class DataSourceConfig {
//    @Value("${spring.shardingsphere.datasource.paul-product.jdbc-url}")
//    private String url0;
//    @Value("${spring.shardingsphere.datasource.paul-product.username}")
//    private String username0;
//    @Value("${spring.shardingsphere.datasource.paul-product.password}")
//    private String password0;
//    @Value("${spring.shardingsphere.datasource.paul-product.driver-class-name}")
//    private String driverClassName0;
//
//    @Value(("${spring.datasource.druid.filters}"))
//    private String filters;
//
//    @Bean("dataSource")
//    public DataSource dataSource() {
//        try {
//            DruidDataSource dataSource0 = new DruidDataSource();
//            dataSource0.setDriverClassName(this.driverClassName0);
//            dataSource0.setUrl(this.url0);
//            dataSource0.setUsername(this.username0);
//            dataSource0.setPassword(this.password0);
//            dataSource0.setFilters(this.filters);
//
//            //分库设置
//            Map<String, DataSource> dataSourceMap = new HashMap<>(1);
//            //添加两个数据库database0和database1
//            dataSourceMap.put("paul-product", dataSource0);
//
//            // 配置 product 表规则
//            TableRuleConfiguration userRuleConfiguration = new TableRuleConfiguration("product", "paul-product.product_$->{0..1}");
//            // 配置分表规则
////            userRuleConfiguration.setTableShardingStrategyConfig(new StandardShardingStrategyConfiguration("id", UserShardingAlgorithm.tableShardingAlgorithm));
//            userRuleConfiguration.setTableShardingStrategyConfig(new InlineShardingStrategyConfiguration("id", "product_${id % 2}"));
//            // 配置分库规则
////            userRuleConfiguration.setDatabaseShardingStrategyConfig(new StandardShardingStrategyConfiguration("id", UserShardingAlgorithm.databaseShardingAlgorithm));
////            userRuleConfiguration.setDatabaseShardingStrategyConfig(new InlineShardingStrategyConfiguration("id", "ds${id % 2}"));
//            // Sharding全局配置
//            ShardingRuleConfiguration shardingRuleConfiguration = new ShardingRuleConfiguration();
//            shardingRuleConfiguration.getTableRuleConfigs().add(userRuleConfiguration);
//            // 创建数据源
//            DataSource dataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfiguration, new Properties());
//            return dataSource;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//}
