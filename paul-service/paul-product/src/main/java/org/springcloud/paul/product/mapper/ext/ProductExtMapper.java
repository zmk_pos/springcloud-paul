package org.springcloud.paul.product.mapper.ext;

import org.springcloud.paul.product.api.bo.BatchInventoryBO;
import org.springcloud.paul.product.api.pojo.Product;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:49
 * @description
 */
public interface ProductExtMapper {
    int updateBatchInventory(List<BatchInventoryBO> list);
}