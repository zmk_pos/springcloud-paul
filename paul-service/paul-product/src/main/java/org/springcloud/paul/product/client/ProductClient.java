package org.springcloud.paul.product.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springcloud.paul.product.api.feign.IProductClient;
import org.springcloud.paul.product.service.IProductService;
import org.springcloud.paul.product.api.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 11:45
 * @description
 */
@Api("产品接口[feign]")
@RestController
@RequestMapping("/product")
@AllArgsConstructor
public class ProductClient implements IProductClient {

    @Autowired
    private IProductService productService;

    @ApiOperation(value = "扣减库存")
    @PostMapping("/subtractStorage")
    public int subtractStorage(@RequestBody List<Product> list) {
        return productService.subtractStorage(list);
    }

    @ApiOperation(value = "根据产品编码获取数据列表")
    @PostMapping("/getProductList")
    public List<Product> getProductList(@RequestBody List<String> list) {
        return productService.getProductListByCode(list);
    }
}
