//package org.springcloud.paul.product.sharding.standard;
//
//import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
//import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
//import org.springframework.stereotype.Component;
//
//import java.util.Collection;
//
///**
// * 按照行分库分表
// * https://www.cnblogs.com/architectforest/p/13639203.html
// *
// * @author zmk
// * @version 1.0
// * @date 2021/4/23 14:42
// * @description
// */
//@Component
//public class OrderTablePreciseShardingAlgorithm implements PreciseShardingAlgorithm<Long> {
//    @Override
//    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Long> shardingValue) {
//        Long curValue = shardingValue.getValue();
//        String curTable = "";
//        if (curValue > 0 && curValue <= 100) {
//            curTable = "product_0";
//        } else if (curValue > 100 && curValue <= 200) {
//            curTable = "product_2";
//        } else if (curValue > 200 && curValue <= 300) {
//            curTable = "product_3";
//        } else {
//            curTable = "product_4";
//        }
//        return curTable;
//    }
//}