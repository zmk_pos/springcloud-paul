package org.springcloud.paul.product.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.common.consts.SystemConst;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.exception.ExceptionCode;
import org.springcloud.paul.product.service.IProductService;
import org.springcloud.paul.product.api.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:22
 * @description
 */
@Api("产品接口")
@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private IProductService productService;

    @ApiOperation(value = "新增记录")
    @PostMapping("/add")
    public int add(@RequestBody Product product) {
        if (null == product) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR);
        }
        return productService.add(product);
    }

    @ApiOperation(value = "更新记录")
    @PostMapping("/update")
    public int update(@RequestBody Product product) {
        if (null == product || product.getId() <= 0) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR);
        }
        return productService.update(product);
    }

    @ApiOperation(value = "根据产品ID获取单条记录")
    @GetMapping("/detail")
    public Product detail(@ApiParam(value = "编号") @RequestParam Long id) {
        return productService.getById(id);
    }

    @ApiOperation(value = "获取数据列表")
    @GetMapping("/list")
    public List<Product> list() {
        return productService.list();
    }

    @ApiOperation(value = "获取分页数据列表")
    @GetMapping("/page")
    public PageResult<Product> pagelist(@ApiParam(value = "当前页码,默认1") @RequestParam(defaultValue = SystemConst.PAGENUM) Integer pageNum
            , @ApiParam(value = "每页大小,默认15") @RequestParam(defaultValue = SystemConst.PAGESIZE) Integer pageSize) {
        return productService.page(pageNum, pageSize);
    }

    @ApiOperation(value = "新增记录")
    @GetMapping("/initAdd")
    public void initAdd() {
        for (int i = 0; i < 1; i++) {
            Product product = new Product();
            product.setCategoryId(0L);
            product.setTitle("苹果" + i);
            product.setFullTitle("青苹果" + i);
            product.setCode("QPG-00" + i);
            product.setPurchasePrice(new BigDecimal(100));
            product.setRecomPrice(new BigDecimal(100));
            product.setInventory(100);
            product.setAbout("1");
            product.setImgUrl("1");
            product.setDetails("1");
            productService.add(product);
        }
    }
}
