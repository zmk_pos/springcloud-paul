package org.springcloud.paul.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.common.key.SnowflakeIdWorker;
import org.springcloud.paul.product.api.bo.BatchInventoryBO;
import org.springcloud.paul.product.config.BaseConfig;
import org.springcloud.paul.product.mapper.ProductMapper;
import org.springcloud.paul.product.mapper.ext.ProductExtMapper;
import org.springcloud.paul.product.api.pojo.Product;
import org.springcloud.paul.product.api.pojo.ProductExample;
import org.springcloud.paul.product.service.IProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:22
 * @description
 */
@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private BaseConfig config;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductExtMapper productExtMapper;


    @Override
    public int add(Product product) {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(config.getSnowFlakeWorkerId(), config.getSnowFlakeDataCenterId());
        long id = snowflakeIdWorker.nextId();
        product.setId(id);
        product.setAddTime(new Date());
        return productMapper.insertSelective(product);
    }

    @Override
    public int update(Product product) {
        return productMapper.updateByPrimaryKey(product);
    }

    @Override
    public Product getById(Long id) {
        return productMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Product> list() {
        return productMapper.selectByExample(null);
    }

    @Override
    public PageResult<Product> page(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Product> list = productMapper.selectByExample(null);
        PageInfo<Product> pageInfo = new PageInfo<>(list);
        return new PageResult<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @Transactional
    @Override
    public int subtractStorage(List<Product> list) {
        if (null == list || list.isEmpty()) {
            throw new RuntimeException("异常:模拟业务异常:Storage branch exception");
        }
//        if (list.stream().filter(product -> product.getCode().equals("aaa-01")).count() > 0) {
//            throw new RuntimeException("异常:模拟业务异常:Storage branch exception");
//        }
//        int j = 0;
//        for (Product item : list) {
//            int i = this.update(item);
//            if (i == 0) {
//                throw new RuntimeException("库存不足");
//            }
//            j++;
//        }
//        return j;

//        List<BatchInventoryBO> boList = new ArrayList<>();
//        for (Product item : list) {
//            BatchInventoryBO bo = new BatchInventoryBO();
//            bo.setId(item.getId());
//            bo.setInventory(item.getInventory());
//            bo.setTableName("product_" + item.getId() % 2);
//            boList.add(bo);
//        }
//        return productExtMapper.updateBatchInventory(boList);
        return update(list.get(0));
    }

    @Override
    public List<Product> getProductListByCode(List<String> list) {
        ProductExample productExample = new ProductExample();
        productExample.createCriteria().andCodeIn(list);
        return productMapper.selectByExample(productExample);
    }
}
