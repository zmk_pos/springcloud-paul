package org.springcloud.paul.product.api.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springcloud.paul.product.api.pojo.Product;
import org.springcloud.paul.product.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/21 14:03
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTest {
    @Autowired
    private IProductService productService;

    @Test
    private void add() {
        for (int i = 0; i < 10; i++) {
            Product product = new Product();
            product.setCategoryId(0L);
            product.setTitle("苹果" + i);
            product.setFullTitle("青苹果" + i);
            product.setCode("QPG-00" + i);
            product.setPurchasePrice(new BigDecimal(100));
            product.setRecomPrice(new BigDecimal(100));
            product.setInventory(100);
            product.setAbout("1");
            product.setImgUrl("1");
            product.setDetails("1");
            int r = productService.add(product);
            assertTrue(r > 0);
        }
    }
}
