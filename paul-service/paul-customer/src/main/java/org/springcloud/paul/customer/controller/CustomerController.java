package org.springcloud.paul.customer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.common.consts.SystemConst;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.exception.ExceptionCode;
import org.springcloud.paul.customer.service.ICustomerService;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/31 13:27
 * @description
 */
@Api("客户接口")
@CrossOrigin
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private ICustomerService customerService;

    @ApiOperation(value = "新增记录")
    @PostMapping("/add")
    public int add(@RequestBody Customers customer) {
        if (null == customer) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR);
        }
        return customerService.add(customer);
    }

    @ApiOperation(value = "更新记录")
    @PostMapping("/update")
    public int update(@RequestBody Customers customer) {
        if (null == customer || customer.getId() <= 0) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR);
        }
        return customerService.update(customer);
    }

    @ApiOperation(value = "获取数据列表")
    @GetMapping("/list")
    public List<Customers> list() {
        return customerService.list();
    }

    @ApiOperation(value = "获取分页数据列表")
    @GetMapping("/page")
    public PageResult<Customers> pagelist(@ApiParam(value = "当前页码,默认1") @RequestParam(defaultValue = SystemConst.PAGENUM) Integer pageNum
            , @ApiParam(value = "每页大小,默认15") @RequestParam(defaultValue = SystemConst.PAGESIZE) Integer pageSize) {
        return customerService.page(pageNum, pageSize);
    }
}
