package org.springcloud.paul.customer.service.impl;

import org.springcloud.paul.common.utils.BigDecimalOperation;
import org.springcloud.paul.customer.mapper.CustomerAccountMapper;
import org.springcloud.paul.customer.api.pojo.CustomerAccount;
import org.springcloud.paul.customer.service.ICustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:42
 * @description
 */
@Service
public class CustomerAccountServiceImpl implements ICustomerAccountService {

    @Autowired
    private CustomerAccountMapper customerAccountMapper;


    @Override
    public int add(CustomerAccount account) {
        return customerAccountMapper.insert(account);
    }

    @Override
    public int update(CustomerAccount account) {
        return customerAccountMapper.updateByPrimaryKey(account);
    }

    @Transactional
    @Override
    public int subtractAccount(Long customerId, double payable) {
        CustomerAccount customerAccount = customerAccountMapper.selectByPrimaryKey(customerId);
        double balance = BigDecimalOperation.sub(customerAccount.getBalance().doubleValue(), payable);
        if (balance < 0) {
            throw new RuntimeException("余额不足");
        }

        customerAccount.setBalance(new BigDecimal(balance));
        return customerAccountMapper.updateByPrimaryKey(customerAccount);
    }
}
