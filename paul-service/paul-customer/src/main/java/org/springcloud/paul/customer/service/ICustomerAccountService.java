package org.springcloud.paul.customer.service;

import org.springcloud.paul.customer.api.pojo.CustomerAccount;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:43
 * @description
 */
public interface ICustomerAccountService {
    int add(CustomerAccount account);

    int update(CustomerAccount account);

    int subtractAccount(Long customerId, double payable);
}
