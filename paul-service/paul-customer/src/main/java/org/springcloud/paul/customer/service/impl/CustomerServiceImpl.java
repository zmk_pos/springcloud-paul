package org.springcloud.paul.customer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.common.key.SnowflakeIdWorker;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.exception.ExceptionCode;
import org.springcloud.paul.customer.mapper.CustomersMapper;
import org.springcloud.paul.customer.config.BaseConfig;
import org.springcloud.paul.customer.api.pojo.CustomerAccount;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springcloud.paul.customer.api.pojo.CustomersExample;
import org.springcloud.paul.customer.service.ICustomerAccountService;
import org.springcloud.paul.customer.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/31 13:27
 * @description
 */
@Service
public class CustomerServiceImpl implements ICustomerService {

    @Autowired
    private CustomersMapper customersMapper;
    @Autowired
    private ICustomerAccountService customerAccountService;
    @Autowired
    private BaseConfig config;

    @Transactional
    @Override
    public int add(Customers customer) {
        CustomersExample customersExample=new CustomersExample();
        customersExample.createCriteria().andTelphoneEqualTo(customer.getTelphone());
        List<Customers> customers = customersMapper.selectByExample(customersExample);
        if (!customers.isEmpty() && customers.size() > 0) {
            throw new BusinessException(ExceptionCode.DATA_REPEAT);
        }

        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(config.getSnowFlakeWorkerId(), config.getSnowFlakeDataCenterId());
        long id = snowflakeIdWorker.nextId();
        customer.setId(id);
        customer.setNickName(customer.getTelphone());
        customer.setSalt("125");
        customer.setAvatar("1");
        customer.setSex("1");
        customer.setStatus((byte) 0);
        customer.setLastLoginTime(new Date());
        customer.setAddTime(new Date());
        customersMapper.insertSelective(customer);
        CustomerAccount account = new CustomerAccount();
        account.setCustomerId(id);
        account.setAmount(new BigDecimal(0));
        account.setBalance(new BigDecimal(0));
        return customerAccountService.add(account);

    }

    @Override
    public int update(Customers customer) {
        customer.setPassword(null);
        customer.setLastLoginTime(null);
        customer.setAddTime(null);
        return customersMapper.updateByPrimaryKeySelective(customer);
    }

    @Override
    public Customers getById(Long id) {
        return customersMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Customers> list() {
        return customersMapper.selectByExample(null);
    }

    @Override
    public PageResult<Customers> page(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Customers> list = customersMapper.selectByExample(null);
        PageInfo<Customers> pageInfo = new PageInfo<>(list);
        return new PageResult<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Customers login(String account, String password) {
        if (StringUtils.isAllBlank(account, password)) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR);
        }
        CustomersExample customersExample = new CustomersExample();
        customersExample.createCriteria().andTelphoneEqualTo(account).andPasswordEqualTo(password);
        Customers customers = customersMapper.selectByExample(customersExample).stream().findAny().orElse(null);
        if (null == customers) {
            throw new BusinessException(ExceptionCode.DATA_NOT_FOUND);
        }
        return customers;
    }
}
