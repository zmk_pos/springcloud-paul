package org.springcloud.paul.customer.exception;

import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import feign.FeignException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.core.bean.utils.HttpContextUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.AccessDeniedException;
import java.util.Base64;
import java.util.Objects;


@RestControllerAdvice
public class ExceptionController {
    private Logger logger = LogManager.getLogger(getClass());
    private final Base64.Encoder encoder = Base64.getEncoder();

    // 捕捉控制器里面自己抛出的所有异常
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseBean> globalException(Exception e) {
//        System.out.println("执行ExceptionController的globalException方法");
//        logger.info("GLOBAL EXCEPTION：" + e);
        HttpServletResponse response = HttpContextUtil.getResponse();
        StringWriter stringWriter = new StringWriter();
        try (PrintWriter printWriter = new PrintWriter(stringWriter)) {
            e.printStackTrace(printWriter);
            printWriter.flush();
            stringWriter.flush();
            String strError = stringWriter.toString().substring(0, 2000);
            Objects.requireNonNull(response).setHeader("error", encoder.encodeToString(strError.getBytes())); //对异常信息进行base64编码
        } catch (Exception ex) {
//            logger.info("error:" + ex);
        }
        String msg = "系统异常，请联系管理员";
        if (e instanceof FlowException || e instanceof FeignException) {
            msg = "限流了";
        } else if (e instanceof DegradeException) {
            msg = "降级了";
        } else if (e instanceof ParamFlowException) {
            msg = "热点参数限流";
        } else if (e instanceof SystemBlockException) {
            msg = "系统规则（负载/...不满足要求）";
        } else if (e instanceof AuthorityException) {
            msg = "授权规则不通过";
        }
        return new ResponseEntity<>(
                new ResponseBean<>(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, null), HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ResponseBean> handleException(AccessDeniedException e) {
//        System.out.println("执行ExceptionController的handleException方法,异常类型：AccessDeniedException");
        return new ResponseEntity<>(
                new ResponseBean<>(
                        HttpStatus.FORBIDDEN.value(), e.getMessage(), null), HttpStatus.FORBIDDEN
        );
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ResponseBean> handleException(BusinessException e) {
//        System.out.println("执行ExceptionController的handleException方法,异常类型：BusinessException");
        return new ResponseEntity<>(
                new ResponseBean<>(
                        e.getCode(), e.getMsg(), e.getData()), HttpStatus.OK
        );
    }

//    private static String getStackTrace(Throwable t) {
//        ByteArrayOutputStream buf = new java.io.ByteArrayOutputStream();
//        t.printStackTrace(new java.io.PrintWriter(buf, true));
//        return buf.toString();
//    }
}
