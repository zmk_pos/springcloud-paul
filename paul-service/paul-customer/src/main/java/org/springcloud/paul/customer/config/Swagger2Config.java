package org.springcloud.paul.customer.config;

import org.springcloud.paul.common.consts.SecureConstant;
import org.springcloud.paul.common.consts.TokenConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/31 13:27
 * @description
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    //是否开启swagger，正式环境一般是需要关闭的，可根据springboot的多环境配置进行设置
//    @Value(value = "${swagger.enabled:true}")
//    Boolean swaggerEnabled;
    @Value("${spring.application.name}")
    String projectName;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(initParams())
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.springcloud.paul"))
                .paths(PathSelectors.any())
                .build();
        //下面这个设置就是在接口的path前加上project-name
//                .pathMapping(projectName);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("路由网关：利用swagger2聚合API文档-" + projectName)
                .description(projectName)
                .termsOfServiceUrl("www.kj-tek.com")
                .version("1.0")
                .build();
    }

    private List<Parameter> initParams() {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(setParameter(SecureConstant.BASIC_HEADER_KEY, TokenConstant.BEARER, "令牌", "string"));
        return parameters;

    }

    private Parameter setParameter(String key, String value, String desc, String type) {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name(key).defaultValue(value).description(desc).modelRef(new ModelRef(type)).parameterType("header").required(false).build();
        return parameterBuilder.build();
    }
}
