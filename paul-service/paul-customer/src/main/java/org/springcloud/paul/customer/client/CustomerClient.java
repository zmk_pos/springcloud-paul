package org.springcloud.paul.customer.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.exception.ExceptionCode;
import org.springcloud.paul.core.bean.response.ResponseBean;
import org.springcloud.paul.customer.api.feign.ICustomerClient;
import org.springcloud.paul.customer.service.ICustomerService;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/2 16:22
 * @description
 */
@Api("客户接口[feign]")
@RestController
@RequestMapping("/customer")
public class CustomerClient implements ICustomerClient {

    @Autowired
    private ICustomerService customerService;

    @ApiOperation(value = "获取用户信息")
    @GetMapping("/login")
    public ResponseBean<Customers> userInfo(String account, String password) {
        Customers customer = customerService.login(account, password);
        return new ResponseBean(200, "", customer);
    }

    @ApiOperation(value = "模拟降级")
    @GetMapping("/degrade")
    public ResponseBean<String> degrade() {
        if (System.currentTimeMillis() % 2 == 0) {
            throw new RuntimeException("random exception");
        }
        return new ResponseBean<>(200, "", "测试降级");
    }

    @ApiOperation(value = "获取单条记录")
    @GetMapping("/get")
    public Customers get(Long id) {
        if (id <= 0) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR);
        }
        return customerService.getById(id);
    }
}
