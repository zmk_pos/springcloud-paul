package org.springcloud.paul.customer.service;

import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.customer.api.pojo.Customers;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/31 13:26
 * @description
 */
public interface ICustomerService {

    int add(Customers customer);

    int update(Customers customer);

    Customers getById(Long id);

    List<Customers> list();

    PageResult<Customers> page(Integer pageNum, Integer pageSize);

    Customers login(String account, String password);
}
