package org.springcloud.paul.customer.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springcloud.paul.customer.api.feign.ICustomerAccountClient;
import org.springcloud.paul.customer.service.ICustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:41
 * @description
 */
@Api("客户资金接口[feign]")
@RestController
@RequestMapping("/customer/account")
@AllArgsConstructor
public class CustomerAccountClient implements ICustomerAccountClient {

    @Autowired
    private ICustomerAccountService customerAccountService;

    @ApiOperation(value = "扣减资金")
    @GetMapping("/subtractAccount")
    public int subtractAccount(Long customerId, double payable) {
        return customerAccountService.subtractAccount(customerId, payable);
    }
}
