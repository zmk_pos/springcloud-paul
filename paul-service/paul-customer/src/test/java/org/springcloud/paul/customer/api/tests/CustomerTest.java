package org.springcloud.paul.customer.api.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springcloud.paul.customer.service.ICustomerService;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * assertArrayEquals(expecteds, actuals)  查看两个数组是否相等。
 * assertEquals(expected, actual) 查看两个对象是否相等。类似于字符串比较使用的equals()方法
 * assertNotEquals(first, second) 查看两个对象是否不相等。
 * assertNull(object) 查看对象是否为空。
 * assertNotNull(object) 查看对象是否不为空。
 * assertSame(expected, actual) 查看两个对象的引用是否相等。类似于使用“==”比较两个对象
 * assertNotSame(unexpected, actual) 查看两个对象的引用是否相等。类似于使用“==”比较两个对象
 * assertTrue(condition) 查看运行结果是否为 true。
 * assertFalse(condition) 查看运行结果是否为 false。
 * assertThat(actual, matcher) 查看实际值是否满足指定的条件。
 * fail() 让测试失败。
 *
 * @author zmk
 * @version 1.0
 * @date 2021/4/21 10:04
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class CustomerTest {
    @Autowired
    private ICustomerService customerService;

    @Test
    public void add() {
            Customers customer = new Customers();
            customer.setTelphone("17788559244");
            customer.setPassword("12356");
            int r = customerService.add(customer);
            assertTrue(r > 0);
    }

    @Test
    public void addBatch() {
        for (int i = 1; i < 100; i++) {
            Customers customer = new Customers();
            customer.setTelphone(String.valueOf((19988559240L + i)));
            customer.setPassword("12356");
            int r = customerService.add(customer);
            assertTrue(r > 0);
        }

    }
}
