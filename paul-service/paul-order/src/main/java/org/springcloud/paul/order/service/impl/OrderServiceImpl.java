package org.springcloud.paul.order.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.common.key.SnowflakeIdWorker;
import org.springcloud.paul.common.utils.BigDecimalOperation;
import org.springcloud.paul.core.bean.exception.BusinessException;
import org.springcloud.paul.core.bean.exception.ExceptionCode;
import org.springcloud.paul.customer.api.feign.ICustomerAccountClient;
import org.springcloud.paul.customer.api.feign.ICustomerClient;
import org.springcloud.paul.order.config.BaseConfig;
import org.springcloud.paul.order.api.dto.CreateOrderDTO;
import org.springcloud.paul.order.api.dto.OrderProductsDTO;
import org.springcloud.paul.order.mapper.OrderProductMapper;
import org.springcloud.paul.order.mapper.OrdersMapper;
import org.springcloud.paul.order.api.pojo.OrderProduct;
import org.springcloud.paul.order.api.pojo.OrderProductExample;
import org.springcloud.paul.order.api.pojo.Orders;
import org.springcloud.paul.order.service.IOrderProductService;
import org.springcloud.paul.order.service.IOrderService;
import org.springcloud.paul.order.api.vo.OrderVO;
import org.springcloud.paul.customer.api.pojo.Customers;
import org.springcloud.paul.product.api.feign.IProductClient;
import org.springcloud.paul.product.api.pojo.Product;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:00
 * @description
 */
@Slf4j
@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private BaseConfig config;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderProductMapper orderProductMapper;

    @Autowired
    private IOrderProductService orderProductService;

    @Autowired
    private IProductClient productClient;
    @Autowired
    private ICustomerClient customerClient;
    @Autowired
    private ICustomerAccountClient customerAccountClient;

    @Override
    public OrderVO getById(Long id) {
        Orders orders = ordersMapper.selectByPrimaryKey(id);
        if (null == orders) {
            return null;
        }
        OrderProductExample productExample = new OrderProductExample();
        productExample.createCriteria().andOrderIdEqualTo(id);
        List<OrderProduct> orderProducts = orderProductMapper.selectByExample(productExample);

        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(orders, orderVO);
        orderVO.setOrderProducts(orderProducts);
        return orderVO;
    }

    @Override
    public List<Orders> list() {
        return ordersMapper.selectByExample(null);
    }

    @Override
    public PageResult<Orders> page(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Orders> list = ordersMapper.selectByExample(null);
        PageInfo<Orders> pageInfo = new PageInfo<>(list);
        return new PageResult<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @Transactional
    @GlobalTransactional(rollbackFor = Exception.class, timeoutMills = 3000)
    @Override
    public Long createOrder(CreateOrderDTO orderDTO) {
        log.info("全局事务开启-----{}", RootContext.getXID());
        Customers customers = customerClient.get(orderDTO.getCustomerId());
        if (null == customers) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "用户信息不存在");
        }
        List<String> productNos = orderDTO.getOrderProducts().stream().map(OrderProductsDTO::getProductNo).collect(Collectors.toList());

        if (productNos.isEmpty()) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "商品编码错误");
        }

        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(config.getSnowFlakeWorkerId(), config.getSnowFlakeDataCenterId());
        Long orderId = snowflakeIdWorker.nextId();
        synchronized (this) {
            List<Product> products = productClient.getProductList(productNos);
            if (null == products || products.isEmpty()) {
                throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "商品信息不存在");
            }

            List<OrderProduct> orderProducts = new ArrayList<>();
            List<Product> productList = new ArrayList<>();
            for (Product product : products) {
                List<OrderProductsDTO> ccollect = orderDTO.getOrderProducts().stream()
                        .filter(orderProductsDTO -> orderProductsDTO.getProductNo().equals(product.getCode()) && orderProductsDTO.getQuantity() <= product.getInventory())
                        .collect(Collectors.toList());
                if (ccollect.isEmpty()) {
                    throw new BusinessException("库存不足");
                }
                OrderProduct orderProduct = new OrderProduct();
                orderProduct.setId(snowflakeIdWorker.nextId());
                orderProduct.setOrderId(orderId);
                orderProduct.setProductId(product.getId());
                orderProduct.setProductNo(product.getCode());
                orderProduct.setProductTitle(product.getTitle());
                orderProduct.setProductImg(product.getImgUrl());
                orderProduct.setSellPrice(product.getRecomPrice());
                orderProduct.setRealPrice(product.getRecomPrice());
                orderProduct.setQuantity(ccollect.get(0).getQuantity());
                orderProduct.setRealAmount(product.getRecomPrice());
                orderProduct.setDisAmount(product.getRecomPrice());
                orderProducts.add(orderProduct);

                product.setInventory(product.getInventory() - ccollect.get(0).getQuantity());
                productList.add(product);
            }

            //计算价格
            double orderSUM;
            double payable = 0;
            for (OrderProduct orderProduct : orderProducts) {
                orderSUM = BigDecimalOperation.mul(orderProduct.getDisAmount().doubleValue(), orderProduct.getQuantity());
                payable = BigDecimalOperation.add(payable, orderSUM);
            }
            String codeRmd = orderId.toString();
            String orderNoStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + codeRmd.substring(0, 6) + codeRmd.substring(codeRmd.length() - 4, codeRmd.length());
            Orders orders = new Orders();
            orders.setId(orderId);
            orders.setOrderNo(orderNoStr);
            orders.setAddTime(new Date());
            orders.setCustomerId(customers.getId());
            orders.setCustomerName(customers.getNickName());
            orders.setStatus((short) 1);
            orders.setAddTime(new Date());
            orders.setDiscountMount(BigDecimal.valueOf(payable));
            orders.setOrderAmount(BigDecimal.valueOf(payable));
            orders.setRealAmount(BigDecimal.valueOf(payable));

            //创建订单
            int order = ordersMapper.insertSelective(orders);
            orderProductService.addOrderProducts(orderProducts);
            if (order <= 0) {
                throw new BusinessException(ExceptionCode.DATA_ERROR.value(), "下单失败");
            }

            //扣减库存
            int storage = productClient.subtractStorage(productList);
            if (storage <= 0) {
                throw new BusinessException(ExceptionCode.DATA_ERROR.value(), "库存不足");
            }
            //扣减金额
            int account = customerAccountClient.subtractAccount(customers.getId(), payable);
            if (account <= 0) {
                throw new BusinessException(ExceptionCode.DATA_ERROR.value(), "余额不足");
            }
//        if (true) {
//            throw new NullPointerException("开始回滚");
//        }
        }
        return orderId;
    }
}
