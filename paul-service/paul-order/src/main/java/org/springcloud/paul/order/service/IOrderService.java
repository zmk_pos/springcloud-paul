package org.springcloud.paul.order.service;

import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.order.api.dto.CreateOrderDTO;
import org.springcloud.paul.order.api.pojo.OrderProduct;
import org.springcloud.paul.order.api.pojo.Orders;
import org.springcloud.paul.order.api.vo.OrderVO;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:00
 * @description
 */
public interface IOrderService {
    OrderVO getById(Long id);

    List<Orders> list();

    PageResult<Orders> page(Integer pageNum, Integer pageSize);

    Long createOrder(CreateOrderDTO orderDTO);
}
