package org.springcloud.paul.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springcloud.paul.common.bean.PageResult;
import org.springcloud.paul.common.consts.SystemConst;
import org.springcloud.paul.common.consts.TokenConstant;
import org.springcloud.paul.order.api.dto.CreateOrderDTO;
import org.springcloud.paul.order.service.IOrderService;
import org.springcloud.paul.order.api.vo.OrderVO;
import org.springcloud.paul.order.api.pojo.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 9:58
 * @description
 */
@Api("订单接口")
@CrossOrigin
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @ApiOperation(value = "获取单条记录")
    @GetMapping("/detail")
    public OrderVO detail(@ApiParam(value = "编号,默认0") @RequestParam(defaultValue = "0") Long id) {
        return orderService.getById(id);
    }

    @ApiOperation(value = "获取数据列表")
    @GetMapping("/list")
    public List<Orders> list() {
        return orderService.list();
    }

    @ApiOperation(value = "获取分页数据列表")
    @GetMapping("/page")
    public PageResult<Orders> pagelist(@ApiParam(value = "当前页码,默认1") @RequestParam(defaultValue = SystemConst.PAGENUM) Integer pageNum
            , @ApiParam(value = "每页大小,默认15") @RequestParam(defaultValue = SystemConst.PAGESIZE) Integer pageSize) {
        return orderService.page(pageNum, pageSize);
    }

    @ApiOperation(value = "下单")
    @PostMapping("/createOrder")
    public Long createOrder(@RequestBody CreateOrderDTO orderDTO, HttpServletRequest request) {
        orderDTO.setCustomerId(Long.valueOf(request.getHeader(TokenConstant.USER_ID)));
        return orderService.createOrder(orderDTO);
    }
}
