package org.springcloud.paul.order.service;

import org.springcloud.paul.order.api.pojo.OrderProduct;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:32
 * @description
 */
public interface IOrderProductService {
    void addOrderProducts(List<OrderProduct> list);
}
