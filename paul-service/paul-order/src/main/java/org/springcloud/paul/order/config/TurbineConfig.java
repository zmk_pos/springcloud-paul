package org.springcloud.paul.order.config;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 集群监控 turbine 配置
 * 监控项目开启：hystrix: dashboard: proxy-stream-allow-list: "*"
 *
 * @author zmk
 * @version 1.0
 * @date 2021/4/25 9:17
 * @description
 */
@Configuration
public class TurbineConfig {
    @Bean
    public ServletRegistrationBean getServlet() {
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/actuator/hystrix.stream");//访问路径
        registrationBean.setName("HystrixMetricsStreamServlet");
        return registrationBean;
    }
}
