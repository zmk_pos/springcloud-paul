package org.springcloud.paul.order;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.alibaba.druid.util.DruidDataSourceUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springcloud.paul.common.consts.AppConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/1 10:05
 * @description
 */
@EnableHystrix
@EnableFeignClients(AppConstant.BASE_PACKAGES)
@EnableDiscoveryClient
@SpringBootApplication(exclude = {
        DruidDataSourceAutoConfigure.class,
        DataSourceAutoConfiguration.class}, scanBasePackages = {"org.springcloud.paul.**"})
@MapperScan(basePackages = "org.springcloud.paul.**.mapper.**")
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}
