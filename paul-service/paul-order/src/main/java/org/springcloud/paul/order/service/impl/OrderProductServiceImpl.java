package org.springcloud.paul.order.service.impl;

import org.springcloud.paul.order.mapper.OrderProductMapper;
import org.springcloud.paul.order.api.pojo.OrderProduct;
import org.springcloud.paul.order.service.IOrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/4/9 12:02
 * @description
 */
@Service
public class OrderProductServiceImpl implements IOrderProductService {
    @Autowired
    private OrderProductMapper orderProductMapper;

    @Override
    public void addOrderProducts(List<OrderProduct> list) {
        for (OrderProduct product : list) {
            orderProductMapper.insertSelective(product);
        }
    }
}
